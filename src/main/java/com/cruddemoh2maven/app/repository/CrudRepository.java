package com.cruddemoh2maven.app.repository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.cruddemoh2maven.app.model.Persona;


@Repository
public class CrudRepository {

	@Autowired
	JdbcTemplate template;

	public List<Persona> lista() {
		return template.query("select id, nombre, dni from persona", new BeanPropertyRowMapper(Persona.class));
	}

	@Transactional
	public int save(Persona p) {

		return template.update("insert into persona(nombre,dni) values (?,?)",
				new Object[] { p.getNombre(), p.getDni() });
	}

	@Transactional
	public int delete(int id) {
		return template.update("delete from persona where id = ? ", new Object[] { id });
	}

	@Transactional
	public int update(Persona p) {
		return template.update("update persona set nombre = ?, dni = ? where id = ? ",
				new Object[] { p.getNombre(), p.getDni(), p.getId() });
	}

}
