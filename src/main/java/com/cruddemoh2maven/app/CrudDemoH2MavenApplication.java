package com.cruddemoh2maven.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CrudDemoH2MavenApplication {

	public static void main(String[] args) {
		SpringApplication.run(CrudDemoH2MavenApplication.class, args);
	}

}
